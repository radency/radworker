﻿using Business.Models;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.ClassMaps
{
    public class PaymentTransactionClassMap : ClassMap<PaymentTransaction>
    {
        public PaymentTransactionClassMap()
        {
            Map(p => p.FirstName).Name("<first_name: string>");
            Map(p => p.LastName).Name(" <last_name: string>");
            Map(p => p.Address).Name(" <address: string>");
            Map(p => p.Payment).Name(" <payment: decimal>");
            Map(p => p.Date).Name(" <date: date>");
            Map(p => p.AccountNumber).Name(" <account_number: long>");
            Map(p => p.Service).Name(" <service: string>");
        }
    }
}
