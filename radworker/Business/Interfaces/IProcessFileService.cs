﻿using Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IProcessFileService
    {
        public Task<ProcessedFileStats> ProcessFileAsync(string fileName, StreamReader fileReader, CancellationToken cancelationToken);
    }
}
