﻿using System;
using Business.Models;
using Coravel.Invocable;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Business.ScheduledJobs
{
    public class ParsingStatsReporter : IInvocable
    {
        private readonly ILogger<ParsingStatsReporter> _logger;
        private readonly IConfiguration _appConfig;
        public ParsingStatsReporter(ILogger<ParsingStatsReporter> logger, IConfiguration config)
        {
            _logger = logger;
            _appConfig = config;
        }
        public async Task Invoke()
        {
            string workStatsPath = Path.Combine(@"..\radworker",_appConfig["WorkStatsPath"]);
            string fileName = "meta.log";
            string outputFilePath = Path.Combine(_appConfig["OutputDirectoryPath"] + "\\" + DateTime.Now.ToString("MM-dd-yyyy"), fileName);

            if (!File.Exists(workStatsPath))
            {
                _logger.LogInformation("No WorkStats file found! Path: {workStatsPath}");
                _logger.LogInformation("Creating creating WorkStats file! Path: {workStatsPath}");
                File.WriteAllText(workStatsPath, JsonConvert.SerializeObject(new WorkerStats(), Formatting.Indented));
                return;
            }

            var currentWorkerStats = File.ReadAllText(workStatsPath);

            var stats = JsonConvert.DeserializeObject<WorkerStats>(currentWorkerStats);

            var currentDate = DateTime.Now.ToShortDateString();

            using (var writer = new StreamWriter(outputFilePath, false))
            {
                _logger.LogInformation($"{currentDate}: Writing meta.log to: {outputFilePath}");
                _logger.LogInformation($"{currentDate}: Parsed Files: {stats.parsed_files}");
                _logger.LogInformation($"{currentDate}: Parsed Lines: {stats.parsed_lines}");
                _logger.LogInformation($"{currentDate}: Found Errors: {stats.found_errors}");
                _logger.LogInformation($"{currentDate}: Invalid Files: " +
                    $"{string.Join(", ", stats?.invalid_files ?? new List<string>())}");
                string text = String.Concat(
                    $"parsed_files: {stats.parsed_files}\n",
                    $"parsed_lines: {stats.parsed_lines}\n",
                    $"found_errors: {stats.found_errors}\n",
                    $"invalid_files: [{string.Join(", ", stats?.invalid_files ?? new List<string>())}]\n");

                writer.WriteLine(text);

                await File.WriteAllTextAsync(workStatsPath, JsonConvert.SerializeObject(new WorkerStats(), Formatting.Indented));
            }
        }
    }
}