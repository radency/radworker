﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class WorkerStats
    {
        public int parsed_files { get; set; }
        public int parsed_lines { get; set; }
        public int found_errors { get; set; }
        public List<string>? invalid_files { get; set; }
    }
}
