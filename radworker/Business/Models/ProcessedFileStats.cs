﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class ProcessedFileStats
    {
        public bool IsValidFile { get; set; } = true;
        public int? ParsedLines { get; set; }
        public int? FoundErrors { get; set; }
        public string? ProcessingResult { get; set; }
    }
}
