﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class CityTransactionsStats
    {
        public string City { get; set; } = null!;
        public IEnumerable<Service>? Services { get; set; }
        public decimal Total { get; set; }
    }
}
