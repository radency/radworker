﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business.Helpers;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;

namespace Business.Models
{
    public class PaymentTransaction
    {
        [Index(0)]
        public string FirstName { get; set; } = null!;

        [Index(1)]
        public string LastName { get; set; } = null!;

        [Index(2)]
        public string Address { get; set; } = null!;

        [Index(3)]
        public decimal Payment { get; set; }

        [Index(4)]
        [JsonConverter(typeof(DateOnlyJsonConverter))]
        public DateOnly Date{ get; set; }

        [Index(5)]
        public long AccountNumber{ get; set; }

        [Index(6)]
        public string Service { get; set; } = null!;
    }
}
