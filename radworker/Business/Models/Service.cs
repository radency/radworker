﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Models
{
    public class Service
    {
        public string Name { get; set; } = null!;
        public IEnumerable<Payer>? Payers { get; set; }
        public decimal Total { get; set; }
    }
}
