﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using System.IO;
using System.Globalization;
using Business.Models;
using Business.Interfaces;
using System.Reflection.PortableExecutable;
using System.Collections.Concurrent;
using Business.ClassMaps;
using CsvHelper.Configuration;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Business.Services
{
    public class ProcessCsvTxtFileService : IProcessFileService
    {
        
        private readonly ILogger<ProcessCsvTxtFileService> _logger;
        public ProcessCsvTxtFileService(ILogger<ProcessCsvTxtFileService> logger)
        {            
            _logger = logger;
        }

        public async Task<ProcessedFileStats> ProcessFileAsync(string fileName, StreamReader fileReader, CancellationToken cancelationToken)
        {
            List<PaymentTransaction> _transactions = new List<PaymentTransaction>();
            List<string> _dirtyRecords = new List<string>();
           
            var jsonOptions = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var result = new ProcessedFileStats();

            var readerConfig = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                NewLine = Environment.NewLine,
                Delimiter = ",",
                HasHeaderRecord = false
            };

            using (var csvReader = new CsvReader(fileReader, readerConfig))
            {
                csvReader.Context.RegisterClassMap<PaymentTransactionClassMap>();
                csvReader.Read();
                
                while (await csvReader.ReadAsync())
                {
                    string rawRecord = JsonConvert
                        .SerializeObject(csvReader.GetRecord<dynamic>(), Formatting.Indented, jsonOptions);

                    try
                    {
                        PaymentTransaction record = csvReader.GetRecord<PaymentTransaction>();

                        if (record.FirstName == String.Empty) 
                            throw new InvalidOperationException("Found a record with empty first name!");
                        if (record.LastName == String.Empty) 
                            throw new InvalidOperationException("Found a record with empty last name!");
                        if (record.Address == String.Empty) 
                            throw new InvalidOperationException("Found a record with empty address!");
                        if (record.Payment == 0)
                            throw new InvalidOperationException("Found a record with the payment of amount 0!");
                        if (record.Date == default(DateOnly)) 
                            throw new InvalidOperationException("Found a record with empty date!");
                        if (record.AccountNumber == 0) 
                            throw new InvalidOperationException("Found a record with invalid account number!");
                        if (record.Service == String.Empty) 
                            throw new InvalidOperationException("Found a record with empty service name!");

                        _transactions.Add(record);
                    }
                    catch (Exception e)
                    {
                        _dirtyRecords.Add(rawRecord);
                        _logger.LogWarning(e.Message);
                        _logger.LogWarning($"Invalid row detected! File: {fileName}\n{@rawRecord}", rawRecord);
                    }
                }
            }

            result.FoundErrors = _dirtyRecords.Count;
            result.ParsedLines = _transactions.Count + result.FoundErrors;
            if(_transactions.Count == 0)
            {
                result.IsValidFile = false;
                return result;
            }             

            var processedResult = _transactions
                .GroupBy(tr => tr.Address.Split(",")[0])
                    .Select(g =>
                        new CityTransactionsStats
                        {
                            City = g.Key,
                            Services = g.GroupBy(t => t.Service).Select(k =>
                            new Service
                            {
                                Name = k.Key,
                                Payers = k.Select(t =>
                                new Payer
                                {
                                    Name = t.FirstName + " " + t.LastName,
                                    Account_number = t.AccountNumber,
                                    Date = t.Date,
                                    Payment = t.Payment
                                }),
                                Total = k.Select(t => t.Payment).Sum()
                            }),
                            Total = g.Select(t => t.Payment).Sum()
                        });

            var resultJson = JsonConvert.SerializeObject(processedResult, Formatting.Indented, jsonOptions);
            result.ProcessingResult = resultJson;
            return result;
        }
    }
}