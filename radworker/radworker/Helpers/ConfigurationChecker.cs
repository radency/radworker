﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace radworker.Helpers
{
    public static class ConfigurationChecker
    {
        public static void IsValidConfiguration(IConfiguration _appConfig)
        {            
            if (!File.Exists("appsettings.json"))
            {
                throw new InvalidOperationException("The configuration file is not available! Can not start the service.");
            }

            else if (new FileInfo("appsettings.json").Length == 0)
            {
                throw new InvalidOperationException("The configuration file is empty! Can not start the service.");
            }

            else if (_appConfig["InputDirectoryPath"] == null || String.IsNullOrWhiteSpace(_appConfig["InputDirectoryPath"]))
            {
                throw new InvalidOperationException(
                    "Input folder for the transactions files has to be defined in appsettings.json. Can't start the service!");
            }

            else if (_appConfig["OutputDirectoryPath"] == null || String.IsNullOrWhiteSpace(_appConfig["OutputDirectoryPath"]))
            {
                throw new InvalidOperationException("Output folder for processing result files has to be defined in appsettings.json.  Can't start the service!");
            }            
        }
    }
}
