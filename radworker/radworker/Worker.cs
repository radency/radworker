using Business.Interfaces;
using Business.Models;
using Microsoft.Extensions.FileSystemGlobbing;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace radworker
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private FileSystemWatcher _csvWatcher;
        private FileSystemWatcher _txtWatcher;
        private readonly IConfiguration _appConfig;
        private readonly IProcessFileService _processFileService;
        private readonly Object _locker = new();

        public Worker(
            ILogger<Worker> logger, 
            IConfiguration config, 
            IProcessFileService processFileService,
            IHostApplicationLifetime hostApplicationLifetime)
        {
            _logger = logger;
            _appConfig = config;
            _processFileService = processFileService;
            _csvWatcher = new FileSystemWatcher(_appConfig["InputDirectoryPath"].ToString(), "*.csv");
            _txtWatcher = new FileSystemWatcher(_appConfig["InputDirectoryPath"].ToString(), "*.txt");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while(!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(500, stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            if (!File.Exists(@".\WorkerStats.json"))
            {
                File.WriteAllText(@".\WorkerStats.json", JsonConvert.SerializeObject(new WorkerStats(), Formatting.Indented));
            }

            _csvWatcher.IncludeSubdirectories = false;
            _csvWatcher.NotifyFilter = 
                NotifyFilters.DirectoryName | 
                NotifyFilters.FileName | 
                NotifyFilters.Size;
            _csvWatcher.Created += OnCreated;
            _csvWatcher.Error += OnError;
            _csvWatcher.EnableRaisingEvents = true;

            _txtWatcher.IncludeSubdirectories = false;
            _txtWatcher.NotifyFilter = 
                NotifyFilters.DirectoryName | 
                NotifyFilters.FileName |
                NotifyFilters.Size;
            _txtWatcher.Created += OnCreated;
            _txtWatcher.Error += OnError;
            _txtWatcher.EnableRaisingEvents = true;

            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping RadWorker Service...");
            return base.StopAsync(cancellationToken);
        }

        private async void OnCreated(object sender, FileSystemEventArgs eventArgs)
        {
            WorkerStats _workerStats = new WorkerStats() { invalid_files = new List<string>() };

            CancellationTokenSource cancellationTokenSource = new();
            var cancelationToken = cancellationTokenSource.Token;

            _logger.LogInformation($"Waiting for the file {eventArgs.FullPath} to be available for reading...");
            await WaitForFileAsync(eventArgs.FullPath, cancelationToken);

            _logger.LogInformation($"New file {eventArgs.FullPath} is available for reading now.");

            ProcessedFileStats processedFileStats;

            using (var streamReader = new StreamReader(eventArgs.FullPath))
            {
                processedFileStats = await _processFileService.ProcessFileAsync(eventArgs.Name, streamReader, cancelationToken);
                if (!processedFileStats.IsValidFile)
                {
                    _logger.LogWarning($"The file {eventArgs.FullPath} is invalid!");
                    if (_workerStats.invalid_files == null)
                    {
                        _workerStats.invalid_files = new List<string>() { eventArgs.FullPath };
                    }
                    else
                    {
                        _workerStats.invalid_files.Add(eventArgs.FullPath);
                    }
                }
                else
                {
                    if(processedFileStats.FoundErrors != null && processedFileStats.FoundErrors > 0)
                        _logger.LogWarning($"The file {eventArgs.FullPath} processed with {processedFileStats.FoundErrors} errors!");
                    else
                        _logger.LogInformation($"The file {eventArgs.FullPath} processed successfully!");
                    _workerStats.found_errors += processedFileStats.FoundErrors ?? 0;
                    _workerStats.parsed_lines += processedFileStats.ParsedLines ?? 0;
                }
            }
            _workerStats.parsed_files++;

            await WriteResultsAsync(processedFileStats, eventArgs);

            await UpdateWorkerStatsAsync(_workerStats);           
        }

        private async void OnError(object sender, ErrorEventArgs eventArgs)
        {
            PrintException(eventArgs.GetException());
        }

        private void PrintException(Exception? ex)
        {
            if (ex != null)
            {
                _logger.LogError($@"Message: {ex.Message}\n""Stacktrace:""\n{ex.StackTrace}");
                PrintException(ex.InnerException);
            }
        }

        private async Task WaitForFileAsync(string filename, CancellationToken stoppingToken)
        {
            bool isFileReady = false;
            while (!isFileReady && !stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using (FileStream inputStream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                        if(inputStream.Length > 0)
                        {
                            isFileReady = true;
                        }
                }
                catch 
                { 
                    //Waiting for a file become available.                        
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            if (stoppingToken.IsCancellationRequested) 
            {
                _logger.LogInformation("Operation has been canceled with the cancellation token.");
            }           

        }

        private async Task UpdateWorkerStatsAsync(WorkerStats workerStats)
        {
            lock (_locker)
            {
                if (!File.Exists(@".\WorkerStats.json"))
                {
                    File.WriteAllText(@".\WorkerStats.json", JsonConvert.SerializeObject(new WorkerStats(), Formatting.Indented));
                }

                string currentWorkerStats = File.ReadAllText(@".\WorkerStats.json");

                var stats = JsonConvert.DeserializeObject<WorkerStats>(currentWorkerStats);
                stats.parsed_lines += workerStats.parsed_lines;
                stats.found_errors += workerStats.found_errors;
                stats.parsed_files += workerStats.parsed_files;
                if (workerStats.invalid_files != null)
                {
                    if (stats.invalid_files == null)
                        stats.invalid_files = new List<string>(workerStats.invalid_files);
                    else
                        stats.invalid_files.AddRange(workerStats.invalid_files);
                }

                File.WriteAllText(@".\WorkerStats.json", JsonConvert.SerializeObject(stats, Formatting.Indented));
            }
        }  
        
        private async Task WriteResultsAsync(ProcessedFileStats processedFileStats, FileSystemEventArgs eventArgs)
        {
            if (!Directory.Exists(Path.Combine(_appConfig["OutputDirectoryPath"], DateTime.Now.Date.ToString())))
            {
                DirectoryInfo outputDirectory = new DirectoryInfo(_appConfig["OutputDirectoryPath"]);
                outputDirectory.CreateSubdirectory(DateTime.Now.ToString("MM-dd-yyyy"));
            }

            string fileName = eventArgs.Name
                .Remove(eventArgs.Name.IndexOf("."))
                .Replace("source", "output") + ".json";
            string filePath = Path.Combine(
                _appConfig["OutputDirectoryPath"] + 
                "\\" + 
                DateTime.Now.ToString("MM-dd-yyyy"), fileName);
            _logger.LogInformation($"Output file path: {filePath}");
            await File.WriteAllTextAsync(filePath, processedFileStats.ProcessingResult);
        }
    }
}