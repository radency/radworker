using radworker;
using Serilog;
using Serilog.Extensions.Logging;
using Business.ScheduledJobs;
using Coravel;
using Business.Interfaces;
using Business.Services;
using Business.Models;
using Newtonsoft.Json;
using radworker.Helpers;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File(
        @"logs\info\log.txt", 
        restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information,
        rollingInterval: RollingInterval.Day)
    .WriteTo.File(
        @"logs\warnings\log.txt",
        restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning,
        rollingInterval: RollingInterval.Day)
    .CreateLogger();

try
{
    Log.Information("Starting RadWorker Service...");
    IHost host = Host.CreateDefaultBuilder(args)
    .UseSerilog()
    .ConfigureServices(services =>
    {       
        services.AddTransient<IProcessFileService, ProcessCsvTxtFileService>();
        services.AddHostedService<Worker>();
        services.AddTransient<ParsingStatsReporter>();
        services.AddScheduler();
        services.Configure<IConfiguration>(options =>
        {
            if (!File.Exists(options["WorkStatsPath"]))
            {
                File.WriteAllText(options["WorkStatsPath"], JsonConvert.SerializeObject(new WorkerStats(), Formatting.Indented));
            }
        });
    })
    .Build();

        host.Services.UseScheduler(scheduler =>
    {
        var jobSchedule = scheduler.Schedule<ParsingStatsReporter>();
        jobSchedule.DailyAtHour(24).PreventOverlapping("ParsingStatsReporter");
    });

    Log.Information("Checking whether the configuration file is available and is valid...");
    ConfigurationChecker.IsValidConfiguration(host.Services.GetRequiredService<IConfiguration>());
    Log.Information("The configuration file is available. Starting the service...");

    await host.RunAsync();
}
catch (Exception exception)
{
    Log.Fatal(exception, "Exception occurred!");
}
finally
{
    Log.Information("Exiting RadWorker Service...");
    Log.CloseAndFlush();
}